package com.tsc.jarinchekhina.tm.api.service;

import com.tsc.jarinchekhina.tm.api.IService;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;

import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IService<Task> {

    void clear(String userId) throws AccessDeniedException;

    List<Task> findAll(String userId) throws AccessDeniedException;

    List<Task> findAll(String userId, Comparator<Task> comparator) throws AccessDeniedException;

    Task add(String userId, Task project) throws AccessDeniedException;

    Task create(String userId, String name) throws AbstractException;

    Task create(String userId, String name, String description) throws AbstractException;

    Task findById(String userId, String id) throws AbstractException;

    Task findByIndex(String userId, Integer index) throws AbstractException;

    Task findByName(String userId, String name) throws AbstractException;

    Task remove(String userId, Task task) throws AccessDeniedException;

    Task removeById(String userId, String id) throws AbstractException;

    Task removeByIndex(String userId, Integer index) throws AbstractException;

    Task removeByName(String userId, String name) throws AbstractException;

    Task updateTaskById(String userId, String id, String name, String description) throws AbstractException;

    Task updateTaskByIndex(String userId, Integer index, String name, String description) throws AbstractException;

    Task startTaskById(String userId, String id) throws AbstractException;

    Task startTaskByIndex(String userId, Integer index) throws AbstractException;

    Task startTaskByName(String userId, String name) throws AbstractException;

    Task finishTaskById(String userId, String id) throws AbstractException;

    Task finishTaskByIndex(String userId, Integer index) throws AbstractException;

    Task finishTaskByName(String userId, String name) throws AbstractException;

    Task changeTaskStatusById(String userId, String id, Status status) throws AbstractException;

    Task changeTaskStatusByIndex(String userId, Integer index, Status status) throws AbstractException;

    Task changeTaskStatusByName(String userId, String name, Status status) throws AbstractException;

}
