package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public void clear(final String userId) {
        List<Project> entitiesNew = new ArrayList<>();
        for (final Project project : entities) {
            if (userId.equals(project.getUserId())) entitiesNew.add(project);
        }
        entities = entitiesNew;
    }

    @Override
    public List<Project> findAll(final String userId) {
        List<Project> projectsByUser = new ArrayList<>();
        for (final Project project : entities) {
            if (userId.equals(project.getUserId())) projectsByUser.add(project);
        }
        if (projectsByUser.size() > 0) return projectsByUser;
        else return null;
    }

    @Override
    public List<Project> findAll(final String userId, final Comparator<Project> comparator) {
        final List<Project> projectsByUser = findAll(userId);
        if (projectsByUser == null) return null;
        final List<Project> projectsSorted = new ArrayList<>(projectsByUser);
        projectsSorted.sort(comparator);
        return projectsSorted;
    }

    @Override
    public Project add(final String userId, final Project project) {
        project.setUserId(userId);
        entities.add(project);
        return project;
    }

    @Override
    public Project findById(final String userId, final String id) {
        if (DataUtil.isEmpty(id)) return null;
        for (final Project project : entities) {
            if (project == null) continue;
            if (!userId.equals(project.getUserId())) continue;
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(final String userId, final Integer index) {
        final List<Project> projectsByUser = findAll(userId);
        return projectsByUser.get(index);
    }

    @Override
    public Project findByName(final String userId, final String name) {
        if (DataUtil.isEmpty(name)) return null;
        for (final Project project : entities) {
            if (project == null) continue;
            if (!userId.equals(project.getUserId())) continue;
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project remove(final String userId, final Project project) {
        if (!userId.equals(project.getUserId())) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeById(final String userId, final String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

    @Override
    public Project removeByName(final String userId, final String name) {
        final Project project = findByName(userId, name);
        if (project == null) return null;
        entities.remove(project);
        return project;
    }

}
