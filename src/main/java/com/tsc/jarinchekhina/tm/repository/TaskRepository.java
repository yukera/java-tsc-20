package com.tsc.jarinchekhina.tm.repository;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void clear(final String userId) {
        List<Task> entitiesNew = new ArrayList<>();
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) entitiesNew.add(task);
        }
        entities = entitiesNew;
    }

    @Override
    public void removeAllByProjectId(final String projectId) {
        List<Task> entitiesNew = new ArrayList<>();
        for (final Task task : entities) {
            if (!projectId.equals(task.getProjectId())) entitiesNew.add(task);
        }
        entities = entitiesNew;
    }

    @Override
    public List<Task> findAll(final String userId) {
        List<Task> tasksByUser = new ArrayList<>();
        for (final Task task : entities) {
            if (userId.equals(task.getUserId())) tasksByUser.add(task);
        }
        if (tasksByUser.size() > 0) return tasksByUser;
        else return null;
    }

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) {
        final List<Task> tasksByUser = findAll(userId);
        if (tasksByUser == null) return null;
        final List<Task> tasksSorted = new ArrayList<>(tasksByUser);
        tasksSorted.sort(comparator);
        return tasksSorted;
    }

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> tasksByProject = new ArrayList<>();
        for (final Task task : entities) {
            if (!userId.equals(task.getUserId())) continue;
            if (projectId.equals(task.getProjectId())) tasksByProject.add(task);
        }
        if (tasksByProject.size() > 0) return tasksByProject;
        else return null;
    }

    @Override
    public Task add(final String userId, final Task task) {
        task.setUserId(userId);
        entities.add(task);
        return task;
    }

    @Override
    public Task findById(final String userId, final String id) {
        if (DataUtil.isEmpty(id)) return null;
        for (final Task task : entities) {
            if (task == null) continue;
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) {
        final List<Task> tasksByUser = findAll(userId);
        return tasksByUser.get(index);
    }

    @Override
    public Task findByName(final String userId, final String name) {
        if (DataUtil.isEmpty(name)) return null;
        for (final Task task : entities) {
            if (task == null) continue;
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task remove(final String userId, final Task task) {
        if (!userId.equals(task.getUserId())) return null;
        entities.remove(task);
        return task;
    }

    @Override
    public Task removeById(final String userId, final String id) {
        final Task task = findById(userId, id);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        entities.remove(task);
        return task;
    }


}
