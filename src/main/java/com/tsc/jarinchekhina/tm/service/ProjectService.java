package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.IProjectRepository;
import com.tsc.jarinchekhina.tm.api.service.IProjectService;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.exception.system.IndexIncorrectException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.Comparator;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        super(projectRepository);
        this.projectRepository = projectRepository;
    }

    @Override
    public void clear(final String userId) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        projectRepository.clear(userId);
    }

    @Override
    public List<Project> findAll(final String userId) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        return projectRepository.findAll(userId);
    }

    @Override
    public List<Project> findAll(final String userId, final Comparator<Project> comparator) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        return projectRepository.findAll(userId, comparator);
    }

    @Override
    public Project add(final String userId, final Project project) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) return null;
        return projectRepository.add(userId, project);
    }

    @Override
    public Project create(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project = new Project();
        project.setName(name);
        projectRepository.add(userId, project);
        return project;
    }

    @Override
    public Project create(final String userId, final String name, final String description) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(userId, project);
        return project;
    }

    @Override
    public Project findById(final String userId, final String id) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.findById(userId, id);
    }

    @Override
    public Project findByIndex(final String userId, final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return projectRepository.findByIndex(userId, index);
    }

    @Override
    public Project findByName(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.findByName(userId, name);
    }

    @Override
    public Project remove(final String userId, final Project project) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (project == null) return null;
        if (!userId.equals(project.getUserId())) throw new AccessDeniedException();
        return projectRepository.remove(userId, project);
    }

    @Override
    public Project removeById(final String userId, final String id) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return projectRepository.removeById(userId, id);
    }

    @Override
    public Project removeByIndex(final String userId, final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        return projectRepository.removeByIndex(userId, index);
    }

    @Override
    public Project removeByName(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return projectRepository.removeByName(userId, name);
    }

    @Override
    public Project updateProjectById(final String userId, final String id, final String name, final String description) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final String userId, final Integer index, final String name, final String description) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(final String userId, final String id) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(final String userId, final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project finishProjectById(final String userId, final String id) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishProjectByIndex(final String userId, final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project finishProjectByName(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final Project project = findById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new IndexIncorrectException();
        final Project project = findByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusByName(final String userId, final String name, final Status status) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Project project = findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
