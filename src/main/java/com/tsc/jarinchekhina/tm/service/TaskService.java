package com.tsc.jarinchekhina.tm.service;

import com.tsc.jarinchekhina.tm.api.repository.ITaskRepository;
import com.tsc.jarinchekhina.tm.api.service.ITaskService;
import com.tsc.jarinchekhina.tm.entity.Task;
import com.tsc.jarinchekhina.tm.enumerated.Status;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyDescriptionException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyIdException;
import com.tsc.jarinchekhina.tm.exception.empty.EmptyNameException;
import com.tsc.jarinchekhina.tm.exception.entity.TaskNotFoundException;
import com.tsc.jarinchekhina.tm.exception.user.AccessDeniedException;
import com.tsc.jarinchekhina.tm.util.DataUtil;

import java.util.Comparator;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void clear(final String userId) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        taskRepository.clear(userId);
    }

    @Override
    public List<Task> findAll(final String userId) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        return taskRepository.findAll(userId);
    }

    @Override
    public List<Task> findAll(final String userId, final Comparator<Task> comparator) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (comparator == null) return null;
        return taskRepository.findAll(userId, comparator);
    }

    @Override
    public Task add(final String userId, final Task task) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) return null;
        return taskRepository.add(userId, task);
    }

    @Override
    public Task create(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public Task create(final String userId, final String name, final String description) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        if (DataUtil.isEmpty(description)) throw new EmptyDescriptionException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
        return task;
    }

    @Override
    public Task findById(final String userId, final String id) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.findById(userId, id);
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        return taskRepository.findByIndex(userId, index);
    }

    @Override
    public Task findByName(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.findByName(userId, name);
    }

    @Override
    public Task remove(final String userId, final Task task) throws AccessDeniedException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (task == null) return null;
        if (!userId.equals(task.getUserId())) throw new AccessDeniedException();
        return taskRepository.remove(userId, task);
    }

    @Override
    public Task removeById(final String userId, final String id) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        return taskRepository.removeById(userId, id);
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        return taskRepository.removeByIndex(userId, index);
    }

    @Override
    public Task removeByName(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        return taskRepository.removeByName(userId, name);
    }

    @Override
    public Task updateTaskById(final String userId, final String id, final String name, final String description) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateTaskByIndex(final String userId, final Integer index, final String name, final String description) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task startTaskById(final String userId, final String id) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByIndex(final String userId, final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startTaskByName(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishTaskById(final String userId, final String id) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByIndex(final String userId, final Integer index) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishTaskByName(final String userId, final String name) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task changeTaskStatusById(final String userId, final String id, final Status status) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(id)) throw new EmptyIdException();
        final Task task = findById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByIndex(final String userId, final Integer index, final Status status) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(index)) throw new EmptyIdException();
        final Task task = findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeTaskStatusByName(final String userId, final String name, final Status status) throws AbstractException {
        if (DataUtil.isEmpty(userId)) throw new AccessDeniedException();
        if (DataUtil.isEmpty(name)) throw new EmptyNameException();
        final Task task = findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        task.setStatus(status);
        return task;
    }

}
