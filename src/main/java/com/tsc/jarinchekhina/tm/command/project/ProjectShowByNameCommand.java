package com.tsc.jarinchekhina.tm.command.project;

import com.tsc.jarinchekhina.tm.command.AbstractProjectCommand;
import com.tsc.jarinchekhina.tm.entity.Project;
import com.tsc.jarinchekhina.tm.exception.AbstractException;
import com.tsc.jarinchekhina.tm.exception.entity.ProjectNotFoundException;
import com.tsc.jarinchekhina.tm.util.TerminalUtil;

public class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-show-by-name";
    }

    @Override
    public String description() {
        return "show project by name";
    }

    @Override
    public void execute() throws AbstractException {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[SHOW PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByName(userId, name);
        if (project == null) throw new ProjectNotFoundException();
        print(project);
    }

}
