package com.tsc.jarinchekhina.tm.exception.user;

import com.tsc.jarinchekhina.tm.exception.AbstractException;

public class EmailRegisteredException extends AbstractException {

    public EmailRegisteredException(String email) {
        super("Error! User with e-mail '" + email + "' is already registered...");
    }

}
